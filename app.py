from flask import Flask, request, Response, redirect
import requests
import time
import json
import os
import zlib
import base64

DATA = os.getenv("DATA", None)

# Counters for the number of successful operations
OAI_PROMPTS = 0
ANT_PROMPTS = 0
MIS_PROMPTS = 0

OPENAI_ALIAS = os.getenv('OPENAI', "openai")
ANTHRO_ALIAS = os.getenv('ANTHROPIC', "anthropic")
MISTRL_ALIAS = os.getenv('MISTRAL', "mistral")

OAI_COMPLETION_ENDPOINT = f'/{OPENAI_ALIAS}/v1/chat/completions';
MIS_COMPLETION_ENDPOINT = f'/{MISTRL_ALIAS}/v1/completions';
ANT_COMPLETION_ENDPOINT = f'/{ANTHRO_ALIAS}/v1/complete';

OAI_MODEL_LIST_ENDPOINT = f'/{OPENAI_ALIAS}/v1/models';
MIS_MODEL_LIST_ENDPOINT = f'/{MISTRL_ALIAS}/v1/models';

OAI_MODEL_IDS = ['gpt-3.5-turbo', 'gpt-3.5-turbo-0301', 'gpt-3.5-turbo-0613', 'gpt-3.5-turbo-16k', 'gpt-3.5-turbo-16k-0613', 'gpt-4', 'gpt-4-0314', 'gpt-4-0613', 'gpt-4-1106-preview', 'gpt-4-vision-preview']
MIS_MODEL_IDS = ['mixtral']

MOEMATE_OAI_API = 'https://generation.moemate.io/api/generation/OPEN_AI_CHAT_COMPLETION/stream'
MOEMATE_ANT_API = 'https://generation.moemate.io/api/generation/CLAUDE_COMPLETION/stream'
MOEMATE_MIS_API = 'https://ai.moemate.io/api/chat'

GENERIC_HEADERS = {'Content-Type': 'application/json', 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.3'}
HEADERS_FOR_MIS = {'WebaHost': 'llm70b.dev.moemate.io'}

start_time = int(time.time())
app = Flask(__name__)

# If DATA isn't none, set vars from DATA.
if DATA is not None: (
    OAI_COMPLETION_ENDPOINT,
    OAI_MODEL_LIST_ENDPOINT,
    ANT_COMPLETION_ENDPOINT,
    MIS_MODEL_LIST_ENDPOINT,
    MIS_COMPLETION_ENDPOINT,
    OAI_MODEL_IDS,
    MIS_MODEL_IDS,
    MOEMATE_OAI_API,
    MOEMATE_ANT_API,
    MOEMATE_MIS_API,
    GENERIC_HEADERS,
    HEADERS_FOR_MIS
) = json.loads(zlib.decompress(base64.b64decode(DATA)).decode())

BLOCKED_IP_ADDRESSES = {"157.230.249.32", "157.245.148.56", "174.138.29.50", "209.97.162.44"}

@app.before_request
def before_request():
    if request.remote_addr in BLOCKED_IP_ADDRESSES:
        return Response("Access denied", status=403)

@app.route(OAI_COMPLETION_ENDPOINT, methods=['POST'])
def proxy_post():
    global OAI_PROMPTS
    # Proxy the POST request to the external API
    response_stream = requests.post(
        MOEMATE_OAI_API,
        json=request.get_json(),
        headers=GENERIC_HEADERS,
        stream=True
    )

    # Increment success counter if request was successful.
    if response_stream.ok:
        OAI_PROMPTS += 1

    # Generator function to stream the response.
    def generate():
        for chunk in response_stream.iter_content(chunk_size=4096):
            yield chunk

    return Response(generate(), content_type=response_stream.headers['Content-Type'], status=response_stream.status_code)

def create_model_listing(model_ids):
    # Creates a JSON response with a list of model information.
    return {"object": "list", "data": [{"id": model_id, "object": "model", "created": 0, "owned_by": "oai"} for model_id in model_ids]}

OAI_MODEL_LIST = create_model_listing(OAI_MODEL_IDS)

@app.route(OAI_MODEL_LIST_ENDPOINT, methods=['GET'])
def list_models():
    # Return a list of models.
    return OAI_MODEL_LIST

@app.route(ANT_COMPLETION_ENDPOINT, methods=['POST'])
def stream_proxy_post():
    global ANT_PROMPTS
    json_data = request.get_json()
    is_streaming = json_data.get("stream", False)
    response_stream = requests.post(
        MOEMATE_ANT_API,
        json=json_data,
        headers=GENERIC_HEADERS,
        stream=True
    )

    if response_stream.ok:
        ANT_PROMPTS += 1

    # Generator function to stream the response
    def generate():
        if is_streaming:
            last_pos = 0
            for line in response_stream.iter_lines():
                if line.startswith(b'data: ') and not line.startswith(b'data: [DONE]'):
                    data = json.loads(line.removeprefix(b'data: ').decode('utf-8'))
                    pos, last_pos = last_pos, len(data['completion'])
                    data['completion'] = data['completion'][pos:]
                    yield f'data: {json.dumps(data, indent=None, separators=(",", ":"))}\r\n\r\n'
        else:
            for chunk in response_stream.iter_content(chunk_size=4096):
                yield chunk

    return Response(generate(), content_type=response_stream.headers['Content-Type'], status=response_stream.status_code)

MIS_MODELS = create_model_listing(MIS_MODEL_IDS)

@app.route(MIS_MODEL_LIST_ENDPOINT, methods=["GET"])
def list_completions():
    # Return a list of completions.
    return MIS_MODELS

@app.route(MIS_COMPLETION_ENDPOINT, methods=['POST'])
def text_completion():
    global MIS_PROMPTS
    request_json = request.get_json()

    if request_json.get("stream", False) or request_json.get("echo", False):
        return Response("cannot use streaming or echo", status=400)

    # Prepare the JSON payload for the external API
    seed = None if request_json.get("seed") == -1 else request_json.get("seed")
    completion_request_json = {
        "inputs": request_json["prompt"],
        "parameters": {
            "best_of": 1,
            "decoder_input_details": True,
            "details": False,
            "do_sample": True,
            "max_new_tokens": request_json["max_new_tokens"],
            "repetition_penalty": request_json["repetition_penalty"],
            "return_full_text": False,
            "seed": seed,
            "stop": request_json["stopping_strings"],
            "stream": False,
            "temperature": request_json["temperature"],
            "top_k": request_json["top_k"],
            "top_p": request_json["top_p"],
            "truncate": None,
            "typical_p": request_json["typical_p"],
            "watermark": False,
        },
    }

    # Make a POST request to the external API
    response_stream = requests.post(
        MOEMATE_MIS_API,
        json=completion_request_json,
        headers={**GENERIC_HEADERS, **HEADERS_FOR_MIS},
    )

    if response_stream.ok:
        MIS_PROMPTS += 1
    else:
        return Response(response_stream.text, status=500)

    # Process the response data
    data = response_stream.json()
    finish_reason = "length" if data["output"]["usage"]["completion_tokens"] >= request_json["max_new_tokens"] else "stop"
    response_data = {
        "id": "cmpl-xyz",
        "object": "text_completion",
        "created": int(time.time()),
        "model": MIS_MODEL_IDS[0],
        "choices": [{
            "index": 0,
            "finish_reason": finish_reason,
            "text": data["output"]["choices"][0]["text"],
            "logprobs": None,
        }],
        "usage": data["output"]["usage"],
    }

    return response_data

landing_page = "https://rentry.org/moepage/raw"
g2lp = os.getenv("REDIRECT", None)
hide_root = os.getenv("HIDE", None)

@app.route('/', methods=['GET'])
def root():
    if hide_root is not None: return '', 204
    if g2lp is not None: redirect(landing_page, code=302)
    uptime = int(time.time()) - start_time
    html_response = f"""
<!doctype html>
<html lang="en">
<head><meta charset="utf-8"><meta name="robots" content="noindex"><title>moe</title></head>
<body style="font-family: sans-serif; background-color: #f0f0f0; padding: 1em;">
<h2>moe</h2>
Uptime: {uptime}<br>
{OPENAI_ALIAS} prompts: {OAI_PROMPTS}<br>
{ANTHRO_ALIAS} prompts: {ANT_PROMPTS}<br>
{MISTRL_ALIAS} prompts: {MIS_PROMPTS}<br><br>
2 minute timeout.<br>
For {MISTRL_ALIAS}: Use ooba, turn off streaming.<br><br>
For {ANTHRO_ALIAS}: Streaming is supported for Claude. Claude 1.2/1.3/2.0 are supported, up to 100K context. Claude-2 and Claude-2.1 redirect to Claude-Instant-1.2.<br>
For {OPENAI_ALIAS}: If you wish to use OpenAI models; Use GPT-4-0314 or GPT-4-0613. GPT-4-1106-Preview redirects to GPT-3.5-Turbo.<br>
<a href="https://gitlab.com/JHVH-0/moe">Click here for the source code.</a>
</body></html>
    """.strip()
    return Response(html_response, mimetype="text/html")

@app.route('/health', methods=['GET'])
def health_check():
    return '', 204

if __name__ == '__main__':
    app.run()
