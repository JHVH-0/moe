FROM python:3.10
WORKDIR /app
COPY ./requirements.txt requirements.txt
RUN pip install --no-cache-dir --upgrade -r requirements.txt
COPY . .
CMD ["gunicorn", "-w", "1", "--worker-class", "gevent", "--timeout", "120", "--access-logfile=-", "--bind", "0.0.0.0:7860", "app:app"]

